<?php

use Latte\Runtime as LR;

/** source: template/template1.latte */
final class Template2c921fc442 extends Latte\Runtime\Template
{
	public const Source = 'template/template1.latte';


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>
        ';
		echo LR\Filters::escapeHtmlText($title) /* line 10 */;
		echo '
    </h1>
</body>
</html>';
	}
}
